<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:95d1c724-0d8c-4464-828b-de798fcd742f(NewLanguage.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="3478db00-baa1-4b99-8a0a-c8301ddc2d7b" name="NewLanguage" version="-1" />
  </languages>
  <imports />
  <registry>
    <language id="3478db00-baa1-4b99-8a0a-c8301ddc2d7b" name="NewLanguage">
      <concept id="1003592845742168925" name="NewLanguage.structure.val" flags="ng" index="Kua4X" />
      <concept id="1003592845742165510" name="NewLanguage.structure.Loop" flags="ng" index="KubhA">
        <child id="1003592845742165850" name="condition" index="KubkU" />
        <child id="1003592845742165853" name="body" index="KubkX" />
      </concept>
      <concept id="1003592845741934824" name="NewLanguage.structure.iterate" flags="ng" index="KvNa8">
        <property id="1003592845741934852" name="body" index="KvNd$" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="KubhA" id="RHuA0X_uiA">
    <node concept="Kua4X" id="RHuA0X_uj3" role="KubkU" />
    <node concept="KvNa8" id="RHuA0X_uj7" role="KubkX">
      <property role="TrG5h" value="name" />
      <property role="KvNd$" value="super" />
    </node>
  </node>
</model>

