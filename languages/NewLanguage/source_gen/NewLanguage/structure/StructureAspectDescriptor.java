package NewLanguage.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  /*package*/ final ConceptDescriptor myConceptAbstractCommand = createDescriptorForAbstractCommand();
  /*package*/ final ConceptDescriptor myConceptLogicalExpression = createDescriptorForLogicalExpression();
  /*package*/ final ConceptDescriptor myConceptLoop = createDescriptorForLoop();
  /*package*/ final ConceptDescriptor myConceptiterate = createDescriptorForiterate();
  /*package*/ final ConceptDescriptor myConceptval = createDescriptorForval();
  private final LanguageConceptSwitch myConceptIndex;

  public StructureAspectDescriptor() {
    myConceptIndex = new LanguageConceptSwitch();
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptAbstractCommand, myConceptLogicalExpression, myConceptLoop, myConceptiterate, myConceptval);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    switch (myConceptIndex.index(id)) {
      case LanguageConceptSwitch.AbstractCommand:
        return myConceptAbstractCommand;
      case LanguageConceptSwitch.LogicalExpression:
        return myConceptLogicalExpression;
      case LanguageConceptSwitch.Loop:
        return myConceptLoop;
      case LanguageConceptSwitch.iterate:
        return myConceptiterate;
      case LanguageConceptSwitch.val:
        return myConceptval;
      default:
        return null;
    }
  }

  /*package*/ int internalIndex(SAbstractConcept c) {
    return myConceptIndex.index(c);
  }

  private static ConceptDescriptor createDescriptorForAbstractCommand() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("NewLanguage", "AbstractCommand", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d9221e8L);
    b.class_(false, true, false);
    b.origin("r:2b518cf2-46ce-4a18-8c4c-feee974456c7(NewLanguage.structure)/1003592845741924840");
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLogicalExpression() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("NewLanguage", "LogicalExpression", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d923558L);
    b.class_(false, true, false);
    b.origin("r:2b518cf2-46ce-4a18-8c4c-feee974456c7(NewLanguage.structure)/1003592845741929816");
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLoop() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("NewLanguage", "Loop", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d95ce06L);
    b.class_(false, false, true);
    b.super_("NewLanguage.structure.AbstractCommand", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d9221e8L);
    b.origin("r:2b518cf2-46ce-4a18-8c4c-feee974456c7(NewLanguage.structure)/1003592845742165510");
    b.aggregate("condition", 0xded7a603d95cf5aL).target(0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d923558L).optional(false).ordered(true).multiple(false).origin("1003592845742165850").done();
    b.aggregate("body", 0xded7a603d95cf5dL).target(0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d9248e8L).optional(true).ordered(true).multiple(true).origin("1003592845742165853").done();
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForiterate() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("NewLanguage", "iterate", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d9248e8L);
    b.class_(false, false, false);
    b.parent(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L);
    b.origin("r:2b518cf2-46ce-4a18-8c4c-feee974456c7(NewLanguage.structure)/1003592845741934824");
    b.prop("body", 0xded7a603d924904L, "1003592845741934852");
    b.alias("iterate");
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForval() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("NewLanguage", "val", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d95db5dL);
    b.class_(false, false, false);
    b.super_("NewLanguage.structure.LogicalExpression", 0x3478db00baa14b99L, 0x8a0ac8301ddc2d7bL, 0xded7a603d923558L);
    b.origin("r:2b518cf2-46ce-4a18-8c4c-feee974456c7(NewLanguage.structure)/1003592845742168925");
    b.alias("val");
    return b.create();
  }
}
